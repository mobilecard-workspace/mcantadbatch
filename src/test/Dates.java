package test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.addcel.colombia.quartz.utils.Utils;

public class Dates {

	public static void main(String[] args) {
		System.out.println(getDateFormated(Utils.getDate()));
	}
	
	private static String getDateFormated(Date date) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		return getDate(df.format(date));
	}
	
	private static String getDate(String date) {
		String d = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar c = Calendar.getInstance();
			c.setTime(sdf.parse(date));
			c.add(Calendar.DATE, -1);
			d = sdf.format(c.getTime());
		} catch (ParseException e) {
			System.out.println(e.getMessage());
		}
		return d;
	}
}
