/*
 * com.addcel.quartz.sftp.util.AddCelSFTPUtil.java	

 *
 * Copyright by AddCel,
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of AddCel 
 *
 * --------------------------------------------------------------------------------------------------------------------------------------------------------
 *								MODIFICATION LOG
 *
 * SE					DATE		RFS#		PURPOSE
 * --------------------------------------------------------------------------------------------------------------------------------------------------------
 * Victor Lira			27/06/2013	NA			Initial Version
 * --------------------------------------------------------------------------------------------------------------------------------------------------------
 */
package com.addcel.antad.quartz.utils;

import java.io.File;
import java.io.FileInputStream;

import com.addcel.antad.quartz.model.SftpVO;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

public class MobilecardSFTPUtil {

	public void sendFileviaSFTP(SftpVO sftpVO) {
		Session session = null;
		Channel channel = null;
		ChannelSftp channelSftp = null;
		JSch jsch = null;
		Log.info(this, "[MC ANTAD BATCH] - *** INICIANDO FTP DE AFD ANTAD ***" + sftpVO.getPathLocal()+sftpVO.getFileName());
		File srcFile = null;
		String resultado = null;
		try {
			srcFile = new File(sftpVO.getPathLocal()+sftpVO.getFileName());
			jsch = new JSch();
			session = jsch.getSession(sftpVO.getUsuario(),
					sftpVO.getHost(), sftpVO.getPuerto());
			session.setPassword(sftpVO.getPassword());
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();
			Log.info(this, "[MC ANTAD BATCH] - CONEXION EXITOSA SFTP");
			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp) channel;
			Log.info(this, "[MC ANTAD BATCH] - INICIANDO ENVIO SFTP ARCHIVO: " +  srcFile.getName());
			channelSftp.put(new FileInputStream(srcFile), srcFile.getName());
			Log.info(this, "[MC ANTAD BATCH] - ENVIO SFTP FINALIZADO ARCHIVO: " +  srcFile.getName());
			resultado = "Exitosa";
		} catch (Exception ex) {
			resultado = "Error: "+ex.getMessage();
			Log.error(this, "[MC ANTAD BATCH] - [ERROR] *** ERROR AL ENVIAR ARCHIVO AFD ANTAD  POR SFTP *** - "
					+ sftpVO.getPathLocal()+sftpVO.getFileName());	
		} finally {
			session.disconnect();
			Log.info(this, "[MC ANTAD BATCH] - TERMINANDO CONEXION SFTP");
			Log.info(this, "[MC ANTAD BATCH] - INICIO NOTIFICACION DE ENVIO");
			AddCelGenericMail.generatedMail(resultado, sftpVO.getPathLocal()+sftpVO.getFileName());
			Log.info(this, "[MC ANTAD BATCH] - FIN NOTIFICACION DE ENVIO");
		}

	}
	

}