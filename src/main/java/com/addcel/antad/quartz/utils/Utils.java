/*
 * com.addcel.quartz.utils.Utils.java	
 *
 * Copyright by AddCel,
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of AddCel 
 *
 * --------------------------------------------------------------------------------------------------------------------------------------------------------
 *								MODIFICATION LOG
 *
 * SE					DATE		RFS#		PURPOSE
 * --------------------------------------------------------------------------------------------------------------------------------------------------------
 * Victor Lira			27/06/2013	NA			Initial Version
 * --------------------------------------------------------------------------------------------------------------------------------------------------------
 */
package com.addcel.antad.quartz.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.springframework.context.ApplicationContext;

public class Utils {

	public static ApplicationContext getApplicationContext(
			JobExecutionContext context, String APPLICATION_CONTEXT_KEY)
			throws JobExecutionException {
		ApplicationContext applicationContext = null;
		try {
			if (applicationContext == null) {
				applicationContext = (ApplicationContext) context
						.getScheduler().getContext()
						.get(APPLICATION_CONTEXT_KEY);
			}
		} catch (SchedulerException e) {
			Log.error(Utils.class, Functions.stackTraceToString(e));
			throw new JobExecutionException(
					"No application context available in scheduler context for key \""
							+ APPLICATION_CONTEXT_KEY + "\"");
		}
		if (applicationContext == null) {
			throw new JobExecutionException(
					"No application context available in scheduler context for key \""
							+ APPLICATION_CONTEXT_KEY + "\"");
		}
		return applicationContext;
	}

	public static String nullToEmpty(String val) {
		if (val != null) {
			return String.valueOf(val);
		}
		return "";
	}

	public static String getDateTime() {
		Log.info("--> getDateTime ");
		String datetime = null;
		try {
			Calendar today = Calendar.getInstance();
			DateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss:SS");
			TimeZone tz = TimeZone.getTimeZone("America/Mexico_City");
			df.setTimeZone(tz);
			datetime = df.format(today.getTime());
		} catch (Exception e) {
			e.printStackTrace();
		}
		Log.info("<-- getDateTime ");
		return datetime;
	}

	public static Date getDate() {
		Log.info("--> getDateTime ");
		return new Date();
	}

	public static String getDateFormated(Date date) {
		// DateFormat df = new SimpleDateFormat("yyyyMMdd");
		DateFormat df = new SimpleDateFormat("ddMMyy");
		return df.format(date);
	}

	public static String getDate(String dateToParse) {
		DateFormat dFormat = new SimpleDateFormat("yyyyMMdd");
		return dFormat.format(getDate());
	}
	
	public static void main(String[] args) {
		try {
			System.out.println(getTransactionDate("16/06/2021 | 04:44:43"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String getTransactionDate(String dateToParse) {
		String date = null;
		DateFormat dFormat = new SimpleDateFormat("yyyyMMdd' 'HH:mm:ss");
		DateFormat transactionDate = new SimpleDateFormat("dd/MM/yyyy | HH:mm:ss");
		try {
			transactionDate.parse(dateToParse);
			date = dFormat.format(transactionDate.parse(dateToParse));
		} catch (ParseException e) {
			
			e.printStackTrace();
		}
		
		return date;
	}

	public static String addSpace(int qty) {
		String space = "";
		for (int i = 0; i < qty; i++)
			space += " ";
		return space;
	}

	public static String add0(int qty) {
		String zero = "";
		for (int i = 0; i < qty; i++)
			zero += "0";
		return zero;
	}

	public static String getFileName(String claveBanco) {
		String path = "/home/rmunoz/GDF_FTP/";
//		String path = "C:\\GDFFiles\\";
		String file = "I" + claveBanco + getDateFormated(new Date()) + ".txt";
		return path + file;
	}
	
	public static String getFecha(String format) {
		Date dt = new Date();
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(dt);
	}
	
	public static String parsePass(String pass)
	  {
	    int len = pass.length();
	    String key = "";
	    for (int i = 0; i < 32 / len; i++) {
	      key = key + pass;
	    }
	    int carry = 0;
	    while (key.length() < 32)
	    {
	      key = key + pass.charAt(carry);
	      carry++;
	    }
	    return key;
	  }
}