package com.addcel.antad.quartz.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.antad.quartz.model.CorreoVO;
import com.google.gson.Gson;


public class AddCelGenericMail {
	
	private static final Logger logger = LoggerFactory.getLogger(AddCelGenericMail.class);
	
	private static final String urlString = "http://localhost:8080/MailSenderAddcel/enviaCorreoAddcel";
			
	public static CorreoVO generatedMail(String status, String srcFile){
		logger.info("[MC ANTAD BATCH] - *** INICIO NOTIFICACION DE ENVIO DE ARCHIVO AFD ***");
		String json = null;
		CorreoVO correo = new CorreoVO();
		String body = null;
		try{
			
			String[] attachments = {};
			String[] bcc = {};
			String[] cc = {};
			body = "Este correo es enviado de forma automatica. "
					+ "El contenido del archivo es generado con todas las transacciones que fueron realizadas en el dia anterior del envio de este mismo correo."
					+ "<br> El resultado de la transferencia es:  "+status;
			String from = "no-reply@addcel.com";
			String subject = "Envio archivo AFD ANTAD " + srcFile;
			String[] to = { "josue.ocampo@addcel.com"};

			correo.setAttachments(attachments);
			correo.setBcc(bcc);
			correo.setCc(cc);
			correo.setBody(body);
			correo.setFrom(from);
			correo.setResultado(status);
			correo.setSubject(subject);
			correo.setTo(to);
			Gson gson = new Gson();
			json = gson.toJson(correo);
			sendMail(json);
			logger.info("[MC ANTAD BATCH] - *** FIN NOTIFICACION DE ENVIO DE ARCHIVO AFD ***");
		}catch(Exception e){
			correo = null;
			logger.error("[MC ANTAD BATCH] - [ERROR] *** ERROR AL ENVIAR LA NOTIFICACION DEL ARCHIVO AFD ANTAD *** - CAUSA: ", e.getCause());
		}
		return correo;
	}
	
	public static void sendMail(String data) {
		String line = null;
		StringBuilder sb = new StringBuilder();
		try {
			logger.info("Iniciando proceso de envio email. ");
			URL url = new URL(urlString);
			logger.info("Conectando con " + urlString);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

			urlConnection.setDoOutput(true);
			urlConnection.setRequestProperty("Content-Type", "application/json");
			urlConnection.setRequestProperty("Accept", "application/json");
			urlConnection.setRequestMethod("POST");

			OutputStreamWriter writter = new OutputStreamWriter(urlConnection.getOutputStream());
			writter.write(data);
			writter.flush();

			logger.info("Datos enviados, esperando respuesta");

			BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}

			logger.info("Respuesta del servidor " + sb.toString());
		} catch (Exception ex) {
			logger.error("Error en: sendMail, al enviar el email ", ex);
		}
	}
}
