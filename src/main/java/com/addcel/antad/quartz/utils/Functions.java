/*
 * com.addcel.quartz.utils.Functions.java	
 *
 * Copyright by AddCel,
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of AddCel 
 *
 * --------------------------------------------------------------------------------------------------------------------------------------------------------
 *								MODIFICATION LOG
 *
 * SE					DATE		RFS#		PURPOSE
 * --------------------------------------------------------------------------------------------------------------------------------------------------------
 * Victor Lira			27/06/2013	NA			Initial Version
 * --------------------------------------------------------------------------------------------------------------------------------------------------------
 */
package com.addcel.antad.quartz.utils;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class Functions {
	public static String stackTraceToString(Exception e) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(baos);
		e.printStackTrace(ps);
		return baos.toString();
	}
}
