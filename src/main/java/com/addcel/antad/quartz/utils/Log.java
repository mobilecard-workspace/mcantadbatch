/*
 * com.addcel.quartz.utils.Log.java	
 *
 * Copyright by AddCel,
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of AddCel 
 *
 * --------------------------------------------------------------------------------------------------------------------------------------------------------
 *								MODIFICATION LOG
 *
 * SE					DATE		RFS#		PURPOSE
 * --------------------------------------------------------------------------------------------------------------------------------------------------------
 * Victor Lira			27/06/2013	NA			Initial Version
 * --------------------------------------------------------------------------------------------------------------------------------------------------------
 */
package com.addcel.antad.quartz.utils;

import org.apache.log4j.Logger;

public class Log {
	static Logger logger = Logger.getLogger(Log.class);

	public static void info(Object clase, String message) {
		String claseMostrar = (clase instanceof String) ? (String) clase : clase.getClass().getName();
		logger.debug("[" + claseMostrar + "] - INFO: " + message);
	}

	public static void debug(Object clase, String message) {
		String claseMostrar = (clase instanceof String) ? (String) clase : clase.getClass().getName();
		logger.debug("[" + claseMostrar + "] - DEBUG: " + message);
	}

	public static void warn(Object clase, String message) {
		String claseMostrar = (clase instanceof String) ? (String) clase : clase.getClass().getName();
		logger.warn("[" + claseMostrar + "] - WARN: " + message);
	}

	public static void error(Object clase, String message) {
		String claseMostrar = (clase instanceof String) ? (String) clase : clase.getClass().getName();
		logger.error("[" + claseMostrar + "] - ERROR: " + message);
	}

	public static void info(String message) {
		info("", message);
	}

	public static void debug(String message) {
		debug("", message);
	}

	public static void warn(String message) {
		warn("", message);
	}

	public static void error(String message) {
		error("", message);
	}

}
