package com.addcel.antad.quartz.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.GeneralSecurityException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UtilsService {
	
	private static final Logger logger = LoggerFactory.getLogger(UtilsService.class);
	
	@Autowired
	private ObjectMapper mapperJk;	
	
	public UtilsService() {
		mapperJk = new ObjectMapper();
	}
	
	public <T> String objectToJson(T object){
		String json=null;
		try {
			json=mapperJk.writeValueAsString(object);
		} catch (IOException e) {
			Log.error("3.- Error al generar json: {}"+e);
		}
		return json;
	}

	public <T> Object jsonToObject(String json, Class<T> clase) {		
		Object obj = null;
		try {
			obj = mapperJk.readValue(json, clase);
		} catch (IOException e) {
			Log.error("Error al parsear cadena json 3: {}"+e);
		}
		return obj;
	}
	
	public String notificacionURLComercio(String urlServicio, String parametros) {
		if(urlServicio.startsWith("https")){
			logger.info("Mandando notificacion de compra por https");
			return notificacionHttpsURLComercio(urlServicio, parametros);
		} else {
			logger.info("Mandando notificacion de compra por http...");
			return notificacionHttpURLComercio(urlServicio, parametros);
		}
	}
	
	public String notificacionHttpURLComercio(String urlServicio, String parametros) {
        InputStream is = null;
        OutputStream os = null;
        InputStreamReader isr = null;
        BufferedWriter writer = null;
        BufferedReader br = null;
        HttpURLConnection conn = null;
        URL url = null;
        String inputLine = null;
        StringBuffer sbf = new StringBuffer();
        try {
        	logger.debug("URL Notificacion: {}", urlServicio);
        	logger.debug("Param Notificacion: {}", parametros);
            url = new URL(urlServicio);
            conn = (HttpURLConnection) url.openConnection();
            if (conn != null) {
                conn.setConnectTimeout(10000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                if (parametros != null) {
                    conn.setDoOutput(true);
                    os = conn.getOutputStream();
                    writer = new BufferedWriter(new OutputStreamWriter(os));
                    writer.write(parametros);
                    writer.close();
                    os.close();
                    conn.connect();
                }
                is = conn.getInputStream();
                isr = new InputStreamReader(is);
                br = new BufferedReader(isr);
                while ((inputLine = br.readLine()) != null) {
                    sbf.append(inputLine);
                }
                logger.debug("Respuesta notificacion: {}", sbf);
            }
        } catch (Exception e) {
         logger.error("Error al leer la url " + url, e);
        } finally {
            if (conn != null) {
                try {
                    conn.disconnect();
                } catch (Exception e) {
                 logger.error("Exception : ", e);
                }
            }
            if (br != null) {
                try {
                    br.close();
                } catch (Exception e) {
                 logger.error("Exception : ", e);
                }
            }

            if (isr != null) {
                try {
                    isr.close();
                } catch (Exception e) {
                 logger.error("Exception : ", e);
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                 logger.error("Exception : ", e);
                }
            }
        }
        return sbf.toString();
    }
	
	public String notificacionHttpsURLComercio(String urlServicio, String parametros) {
		InputStream is = null;
        OutputStream os = null;
        InputStreamReader isr = null;
        BufferedWriter writer = null;
        BufferedReader br = null;
        HttpURLConnection conn = null;
		URL url = null;
		StringBuffer response = new StringBuffer(); 
		String inputLine = null;
        StringBuffer sbf = new StringBuffer();
        
		HostnameVerifier hv = new HostnameVerifier() {
			public boolean verify(String urlHostName, SSLSession session) {
				System.out.println("Warning: URL Host: " + urlHostName
						+ " vs. " + session.getPeerHost());
				return true;
			}
		};	        
	    
        try {
        	trustAllHttpsCertificates();
    	    HttpsURLConnection.setDefaultHostnameVerifier(hv);
            url = new URL(urlServicio);
            conn = (HttpURLConnection) url.openConnection();
            if (conn != null) {
                conn.setConnectTimeout(10000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                if (parametros != null) {
                    conn.setDoOutput(true);
                    os = conn.getOutputStream();
                    writer = new BufferedWriter(new OutputStreamWriter(os));
                    writer.write(parametros);
                    writer.close();
                    os.close();
                    conn.connect();
                }
                is = conn.getInputStream();
                isr = new InputStreamReader(is);
                br = new BufferedReader(isr);
                while ((inputLine = br.readLine()) != null) {
                    sbf.append(inputLine);
                }
                logger.debug("Respuesta notificacion: {}", sbf);
            }
        } catch (GeneralSecurityException e) {
        	logger.info("Error Al generar certificados...");
        	e.printStackTrace();
        }catch (MalformedURLException e) {
        	logger.info("error al conectar con la URL: "+urlServicio);
        	e.printStackTrace();
        }catch (ProtocolException e) {
        	logger.info("Error al enviar datos.. ");
			e.printStackTrace();
		} catch (IOException e) {
			logger.info("Error al algo... ");
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
            if (conn != null) {
                try {
                    conn.disconnect();
                } catch (Exception e) {
                 logger.error("Exception : ", e);
                }
            }
            if (br != null) {
                try {
                    br.close();
                } catch (Exception e) {
                 logger.error("Exception : ", e);
                }
            }

            if (isr != null) {
                try {
                    isr.close();
                } catch (Exception e) {
                 logger.error("Exception : ", e);
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                 logger.error("Exception : ", e);
                }
            }
        }
        System.out.println(response.toString());
        return ""; 
	}
	
	public static class miTM implements javax.net.ssl.TrustManager,
			javax.net.ssl.X509TrustManager {
		public java.security.cert.X509Certificate[] getAcceptedIssuers() {
			return null;
		}

		public boolean isServerTrusted(
				java.security.cert.X509Certificate[] certs) {
			return true;
		}

		public boolean isClientTrusted(
				java.security.cert.X509Certificate[] certs) {
			return true;
		}

		public void checkServerTrusted(
				java.security.cert.X509Certificate[] certs, String authType)
				throws java.security.cert.CertificateException {
			return;
		}

		public void checkClientTrusted(
				java.security.cert.X509Certificate[] certs, String authType)
				throws java.security.cert.CertificateException {
			return;
		}
	}

	private static void trustAllHttpsCertificates() throws Exception {

		javax.net.ssl.TrustManager[] trustAllCerts =

		new javax.net.ssl.TrustManager[1];

		javax.net.ssl.TrustManager tm = new miTM();

		trustAllCerts[0] = tm;

		javax.net.ssl.SSLContext sc =

		javax.net.ssl.SSLContext.getInstance("SSL");

		sc.init(null, trustAllCerts, null);

		javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(

		sc.getSocketFactory());

	}
}
