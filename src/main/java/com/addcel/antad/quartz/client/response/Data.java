package com.addcel.antad.quartz.client.response;

public class Data {
	
	private boolean success;
	
	private TransactionMC11 data[];

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public TransactionMC11[] getData() {
		return data;
	}

	public void setData(TransactionMC11[] data) {
		this.data = data;
	}
	
}
