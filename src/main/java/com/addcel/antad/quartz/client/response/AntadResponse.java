package com.addcel.antad.quartz.client.response;

public class AntadResponse {
	
	private boolean success;
	private AntadData data;
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public AntadData getData() {
		return data;
	}
	public void setData(AntadData data) {
		this.data = data;
	}
}