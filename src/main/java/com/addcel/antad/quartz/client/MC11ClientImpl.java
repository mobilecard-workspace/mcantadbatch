package com.addcel.antad.quartz.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.addcel.antad.quartz.client.response.Data;
import com.addcel.antad.quartz.client.response.DataRequest;
import com.addcel.antad.quartz.client.response.Request;
import com.google.gson.Gson;

@Service
public class MC11ClientImpl{

	private static final Logger LOGGER = LoggerFactory.getLogger(MC11ClientImpl.class);
	
	@Autowired
    private RestTemplate restTemplate;
	
	private static final String PATH_KD = "https://hdvbyghgt1.execute-api.us-east-1.amazonaws.com/public/metadata/antad-servicespayments";
	
	private Gson GSON = new Gson();
	
	public Data getTransactions(String startDate, String endDate) {
		DataRequest request = new DataRequest();
		Data transactions = null;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.add("x-api-key", "4XCQhSZIfo6Xtf9hXqLRN5LLXwvykjUS2u619r5c");
            headers.setContentType(MediaType.APPLICATION_JSON);
            request.setParameters(new Request());
            request.getParameters().setStartDate(startDate);
            request.getParameters().setEndDate(endDate);
            HttpEntity<DataRequest> requestEntity = new HttpEntity<DataRequest>(request,headers);
            LOGGER.info("REQUEST TO METADATA: {}", GSON.toJson(requestEntity));
            ResponseEntity<Data> response = restTemplate.exchange(PATH_KD, HttpMethod.POST, requestEntity, Data.class);
            LOGGER.debug("Respuesta del servicio Metadata - {} ", GSON.toJson(response));
            transactions = response.getBody();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return transactions;
	}

}
