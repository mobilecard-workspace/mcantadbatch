package com.addcel.antad.quartz.client.response;

public class DataRequest {
	
	private Request parameters;

	public Request getParameters() {
		return parameters;
	}

	public void setParameters(Request parameters) {
		this.parameters = parameters;
	}

}
