package com.addcel.antad.quartz.client.response;

public class TransactionMC11 {
	
	private String id;
	
	private String lid;
	
	private Long on;
	
	private String onAsString;
	
	private String reference;
	
	private String emitter;
	
	private double amount;
	
	private double commission;
	
	private String sourceCard;
	
	private String sourceCardSuffix;
	
	private String folioBalanceInquiry;
	
	private String status;
	
	private String response;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLid() {
		return lid;
	}

	public void setLid(String lid) {
		this.lid = lid;
	}

	public Long getOn() {
		return on;
	}

	public void setOn(Long on) {
		this.on = on;
	}

	public String getOnAsString() {
		return onAsString;
	}

	public void setOnAsString(String onAsString) {
		this.onAsString = onAsString;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getEmitter() {
		return emitter;
	}

	public void setEmitter(String emitter) {
		this.emitter = emitter;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getCommission() {
		return commission;
	}

	public void setCommission(double commission) {
		this.commission = commission;
	}

	public String getSourceCard() {
		return sourceCard;
	}

	public void setSourceCard(String sourceCard) {
		this.sourceCard = sourceCard;
	}

	public String getSourceCardSuffix() {
		return sourceCardSuffix;
	}

	public void setSourceCardSuffix(String sourceCardSuffix) {
		this.sourceCardSuffix = sourceCardSuffix;
	}

	public String getFolioBalanceInquiry() {
		return folioBalanceInquiry;
	}

	public void setFolioBalanceInquiry(String folioBalanceInquiry) {
		this.folioBalanceInquiry = folioBalanceInquiry;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

}
