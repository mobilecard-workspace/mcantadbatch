package com.addcel.antad.quartz.client.response;

public class AntadData {
	private String RespCode;
	private Integer NumAuth;
	private String MensajeTicket;
	private String MensajeCajero;
	private String FolioTransaccion;
	private String FolioComercio;
	private Integer Comision;
	
	public String getRespCode() {
		return RespCode;
	}
	public void setRespCode(String respCode) {
		RespCode = respCode;
	}
	public Integer getNumAuth() {
		return NumAuth;
	}
	public void setNumAuth(Integer numAuth) {
		NumAuth = numAuth;
	}
	public String getMensajeTicket() {
		return MensajeTicket;
	}
	public void setMensajeTicket(String mensajeTicket) {
		MensajeTicket = mensajeTicket;
	}
	public String getMensajeCajero() {
		return MensajeCajero;
	}
	public void setMensajeCajero(String mensajeCajero) {
		MensajeCajero = mensajeCajero;
	}
	public String getFolioTransaccion() {
		return FolioTransaccion;
	}
	public void setFolioTransaccion(String folioTransaccion) {
		FolioTransaccion = folioTransaccion;
	}
	public String getFolioComercio() {
		return FolioComercio;
	}
	public void setFolioComercio(String folioComercio) {
		FolioComercio = folioComercio;
	}
	public Integer getComision() {
		return Comision;
	}
	public void setComision(Integer comision) {
		Comision = comision;
	}
	
}
