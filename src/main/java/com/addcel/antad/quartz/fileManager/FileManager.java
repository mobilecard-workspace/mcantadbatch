package com.addcel.antad.quartz.fileManager;

import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

import com.addcel.antad.quartz.model.AntadCredentials;
import com.addcel.antad.quartz.model.TransaccionDetalleVO;
import com.addcel.antad.quartz.utils.Log;
import com.addcel.antad.quartz.utils.Utils;

import crypto.Crypto;

public class FileManager {

	private String fileName;
	
	public String makeFile(List<TransaccionDetalleVO> detalleList, AntadCredentials credentials) {
		PrintWriter pw = null;
		int consecutivo = 1;
		double totalImporte = 0;
		double totalComision = 0;
		Log.info(this, "[MC ANTAD BATCH] - *** CREANDO ARCHIVO AFD ANTAD ***");
		String fileName = new String();
		String linea = null; 
		try {
			fileName = "/usr/java/antad/files/"+creaNombreArchivo(credentials);
			//fileName = "D:/archivo/"+creaNombreArchivo(credentials);
			pw = new PrintWriter(fileName, "UTF-8");
			// INSERTA LA CABECERA DEL ARCHIVO
			pw.write("1"+completaFormatoCeros(credentials.getComercio(), 5)+Utils.getFecha("yyyyMMdd")+"0210"+"\n");
			for(TransaccionDetalleVO detalle : detalleList){
				linea = generaRegistroTransaccion(detalle,  consecutivo, credentials);
				Log.info(this, "[MC ANTAD BATCH] - REGISTRANDO DETALLE DE TRANSACCION:  "+linea);
				pw.write(linea+"\n");
				totalImporte = totalImporte + Double.valueOf(detalle.getImporte());
				totalComision = totalComision + detalle.getComision();
				consecutivo++;
			}
			// INSERTA LA FIN DEL ARCHIVO
			pw.write("3"+completaFormatoCeros(detalleList.size(), 7)+completaFormatoCeros(totalImporte, 12)
					+completaFormatoCeros(totalComision, 12)+"\n");
			pw.flush();
			pw.close();
			Log.info(this, "[MC ANTAD BATCH] - *** FIN DE CREACION DE ARCHIVO AFD ANTAD ***");
		} catch (Exception ex) {
			Log.error(this, "[MC ANTAD BATCH] - [ERROR] *** ERROR AL CREAR ARCHIVO AFD ANTAD *** - "+
					"1"+completaFormatoCeros(credentials.getComercio(), 5)+Utils.getFecha("yyyyMMdd")+"0210"+" - CAUSA: "+ ex.getCause());			
			ex.printStackTrace();
		}
		return fileName;
	}
	
	private String creaNombreArchivo(AntadCredentials credentials) {
		StringBuffer nombre = new StringBuffer();
		try {
			nombre.append("C");
			nombre.append(completaFormatoCeros(credentials.getComercio(), 5));
			nombre.append(Utils.getFecha("yyyyMMdd"));
			nombre.append(".");
			nombre.append("001");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return nombre.toString();
	}
	private String generaRegistroTransaccion(TransaccionDetalleVO detalle, int consecutivo, AntadCredentials credentials) {
		String lineaArchivo = new String();
		try {
			lineaArchivo = lineaArchivo + "2";
			Log.debug(lineaArchivo);
			
			lineaArchivo = lineaArchivo + completaFormatoCeros(consecutivo, 7);
			Log.debug(lineaArchivo);
			
			lineaArchivo = lineaArchivo + detalle.getFechaTransaccion();
			Log.debug(lineaArchivo);
			Log.debug("ID EMISOR: "+detalle.getIdEmisor());
			if("95".equals(detalle.getIdEmisor()) || "96".equals(detalle.getIdEmisor()) ||
					"92".equals(detalle.getIdEmisor()) || "94".equals(detalle.getIdEmisor())
					|| "93".equals(detalle.getIdEmisor())) {
				lineaArchivo = lineaArchivo + "000033";
			} else {
				lineaArchivo = lineaArchivo + "000031";
			}
			Log.debug(lineaArchivo);
			
			lineaArchivo = lineaArchivo + completaFormatoReferencia(detalle.getReferencia(), 50);
			Log.debug(lineaArchivo);
			
			lineaArchivo = lineaArchivo + completaFormatoCeros(detalle.getIdEmisor(), 5);
			Log.debug(lineaArchivo);
			
			lineaArchivo = lineaArchivo + completaFormatoCeros(credentials.getSucursal(), 15);
			Log.debug(lineaArchivo);
			
			lineaArchivo = lineaArchivo + completaFormatoCeros(credentials.getCaja(), 12);
			Log.debug(lineaArchivo);
			
			lineaArchivo = lineaArchivo + completaFormatoCeros(credentials.getCajero(), 12);
			Log.debug(lineaArchivo);
			
			lineaArchivo = lineaArchivo + completaFormatoCeros(detalle.getImporte(), 16);
			Log.debug(lineaArchivo);
			
			lineaArchivo = lineaArchivo + completaFormatoCeros(detalle.getComision(), 16);
			Log.debug(lineaArchivo);
			
			lineaArchivo = lineaArchivo + completaFormatoCeros(detalle.getNumAutorizacion(), 6);
			Log.debug(lineaArchivo);
			
			lineaArchivo = lineaArchivo + completaFormatoCeros(detalle.getFolioComercio(), 12);
			Log.debug(lineaArchivo);
			
			lineaArchivo = lineaArchivo + completaFormatoCeros(detalle.getFolioTransaccion(), 12);
			Log.debug(lineaArchivo);
			
			lineaArchivo = lineaArchivo + "2";
			Log.debug(lineaArchivo);
			
			lineaArchivo = lineaArchivo + "E";
			Log.debug(lineaArchivo);
			
			lineaArchivo = lineaArchivo + detalle.getCodigoRespuesta();
			Log.debug(lineaArchivo);

			lineaArchivo = lineaArchivo + Crypto.aesDecrypt(Utils.parsePass("5525963513"), detalle.getNumTarjeta());
			Log.debug(lineaArchivo);
		} catch (Exception e) {
			Log.error(this, "[MC ANTAD BATCH] - ERROR AL GENERAR EL DETALLE DE LA TRANSACCION - "+
					detalle.getIdTransaccion()+" - CAUSA: "+ e.getCause());
			e.printStackTrace();
		}
		return lineaArchivo;
	}

	private String completaFormatoReferencia(String referencia, int length) {
		StringBuffer conBuffer = new StringBuffer();
		try {
			conBuffer.append(referencia);
			for(int i = referencia.length(); i < length; i++){
				conBuffer.append(" ");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conBuffer.toString();
	}

	private String completaFormatoCeros(String dato, int length) {
		StringBuffer conBuffer = new StringBuffer();
		try {
			if(dato == null){
				dato = "0";
			}
			dato = dato.replace(".", "");
			for(int i = dato.length(); i < length; i++){
				conBuffer.append("0");
			}
			conBuffer.append(dato);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conBuffer.toString();
	}
	
	private String completaFormatoCeros(double dato, int length) {
		StringBuffer conBuffer = new StringBuffer();
		String con = null;
		try {
			NumberFormat formatter = new DecimalFormat("#0.00");     
			con = String.valueOf(formatter.format(dato));
			con = con.replace(".", "");
			for(int i = con.length(); i < length; i++){
				conBuffer.append("0");
			}
			conBuffer.append(con);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conBuffer.toString();
	}
	
	private String completaFormatoCeros(int dato, int length) {
		StringBuffer conBuffer = new StringBuffer();
		String con = null;
		try {
			con = String.valueOf(dato);
			for(int i = con.length(); i < length; i++){
				conBuffer.append("0");
			}
			conBuffer.append(con);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conBuffer.toString();
	}

	public static void main(String[] args) {
		FileManager file = new FileManager();
		String consecutivo = file.completaFormatoReferencia("474321412200", 50);
		System.out.println(consecutivo);
		System.out.println(consecutivo.length());
	}

	public String getFilePath() {
		return fileName;
	}
}