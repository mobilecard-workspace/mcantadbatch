package com.addcel.antad.quartz.job;

import org.quartz.InterruptableJob;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.UnableToInterruptJobException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.JobDetailBean;

import com.addcel.antad.quartz.services.MCAntadService;
import com.addcel.antad.quartz.utils.Log;
import com.addcel.antad.quartz.utils.Utils;

public class MCAntadJob extends JobDetailBean implements InterruptableJob  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6617177006957233680L;

	@Autowired
	private MCAntadService service;
	
	private ApplicationContext applicationContext;
	
	@Override
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		Log.info(this, "[MC ANTAD BATCH] - *** INICIANDO JOB DE AFD ANTAD ***");
		applicationContext = applicationContext == null ? Utils.getApplicationContext(context, "applicationContext") : applicationContext;
		service = (MCAntadService) applicationContext.getBean("AntadService");	
		service.iniciaSFTP(context);	
		Log.info(this, "[MC ANTAD BATCH] - *** FINALIZANDO JOB DE AFD ANTAD ***");
	}

	@Override
	public void interrupt() throws UnableToInterruptJobException {
		
		
	}
	
}
