package com.addcel.antad.quartz.model;

public class TransaccionFinVO {

	private String tipoRegistro;
	
	private int totalRegistros;
	
	private double totalImporte;
	
	private double totalComision;

	public String getTipoRegistro() {
		return tipoRegistro;
	}

	public void setTipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}

	public int getTotalRegistros() {
		return totalRegistros;
	}

	public void setTotalRegistros(int totalRegistros) {
		this.totalRegistros = totalRegistros;
	}

	public double getTotalImporte() {
		return totalImporte;
	}

	public void setTotalImporte(double totalImporte) {
		this.totalImporte = totalImporte;
	}

	public double getTotalComision() {
		return totalComision;
	}

	public void setTotalComision(double totalComision) {
		this.totalComision = totalComision;
	}
	
}
