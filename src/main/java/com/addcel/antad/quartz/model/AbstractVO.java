/*
 * com.addcel.quartz.model.AbstractVO.java	
 *
 * Copyright by AddCel,
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of AddCel 
 *
 * --------------------------------------------------------------------------------------------------------------------------------------------------------
 *								MODIFICATION LOG
 *
 * SE					DATE		RFS#		PURPOSE
 * --------------------------------------------------------------------------------------------------------------------------------------------------------
 * Victor Lira			27/06/2013	NA			Initial Version
 * --------------------------------------------------------------------------------------------------------------------------------------------------------
 */
package com.addcel.antad.quartz.model;

import java.io.Serializable;
import java.lang.reflect.Method;

import com.addcel.antad.quartz.utils.Functions;
import com.addcel.antad.quartz.utils.Log;

public class AbstractVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9093255136891074876L;

	private static String ipLocal = "";
	
	private int idError;
	
	private String mensajeError;

	/**
	 * @return Devuelve ipLocal.
	 */
	public static String getIpLocal() {
		return AbstractVO.ipLocal;
	}

	/**
	 * @param ipLocal
	 *            El ipLocal a establecer.
	 */
	public static void setIpLocal(String ipLocal) {
		AbstractVO.ipLocal = ipLocal;
	}


	public String IPAddress = null;

	/**
	 * @return Devuelve iPAddress.
	 */
	public String getIPAddress() {
		return IPAddress;
	}

	/**
	 * @param address
	 *            El iPAddress a establecer.
	 */
	public void setIPAddress(String address) {
		IPAddress = address;
	}

	/**
	 * Returns the methods that does not need a parameter And the value that
	 * those methods return It is useful in debugging. For examplo
	 * System.out.println("user = " + userVO);
	 * 
	 * @return
	 */
	public String toString() {
		StringBuffer resultado = new StringBuffer();
		Class myClass = this.getClass();
		Method[] metodos = myClass.getDeclaredMethods();
		int methodsLength = metodos.length;
		try {
			for (int i = 0; i < methodsLength; i++) {
				if (metodos[i].getParameterTypes().length == 0) {
					resultado.append(" ");
					resultado.append(metodos[i].getName());
					resultado.append(": ");
					resultado.append(metodos[i].invoke(this, null));
					resultado.append(" \n");
				}
			}
		} catch (Exception e) {
			Log.error(Functions.stackTraceToString(e));
		}
		return resultado.toString();
	}

	public int getIdError() {
		return idError;
	}

	public void setIdError(int idError) {
		this.idError = idError;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
}
