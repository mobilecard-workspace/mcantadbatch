package com.addcel.antad.quartz.model.mapper;

import java.util.List;

import com.addcel.antad.quartz.model.AntadCredentials;
import com.addcel.antad.quartz.model.SftpVO;
import com.addcel.antad.quartz.model.TransaccionDetalleVO;

public interface MCAntadBatchMapper {
	
	public List<TransaccionDetalleVO> getTransaccionesAntad();
	
	public List<TransaccionDetalleVO> getTransaccionesFallidas();

	public SftpVO getSFTP(String string);
	
	public AntadCredentials getAntadCredentials();
	
}
