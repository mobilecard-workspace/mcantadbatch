/*
 * com.addcel.quartz.sftp.vo.AddCelSFTPVO.java	
 *
 * Copyright by AddCel,
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of AddCel 
 *
 * --------------------------------------------------------------------------------------------------------------------------------------------------------
 *								MODIFICATION LOG
 *
 * SE					DATE		RFS#		PURPOSE
 * --------------------------------------------------------------------------------------------------------------------------------------------------------
 * Victor Lira			27/06/2013	NA			Initial Version
 * --------------------------------------------------------------------------------------------------------------------------------------------------------
 */
package com.addcel.antad.quartz.model;

import com.addcel.antad.quartz.model.AbstractVO;

public class SftpVO extends AbstractVO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String usuario;
	
	private String password;
	
	private String host;
	
	private int puerto;
	
	private String pathLocal;
	
	private String pathRemote;
	
	private String fileName;

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPuerto() {
		return puerto;
	}

	public void setPuerto(int puerto) {
		this.puerto = puerto;
	}

	public String getPathLocal() {
		return pathLocal;
	}

	public void setPathLocal(String pathLocal) {
		this.pathLocal = pathLocal;
	}

	public String getPathRemote() {
		return pathRemote;
	}

	public void setPathRemote(String pathRemote) {
		this.pathRemote = pathRemote;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

}
