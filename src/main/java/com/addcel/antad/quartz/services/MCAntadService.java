package com.addcel.antad.quartz.services;


import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.antad.quartz.client.MC11ClientImpl;
import com.addcel.antad.quartz.client.response.AntadData;
import com.addcel.antad.quartz.client.response.AntadResponse;
import com.addcel.antad.quartz.client.response.Data;
import com.addcel.antad.quartz.client.response.TransactionMC11;
import com.addcel.antad.quartz.fileManager.FileManager;
import com.addcel.antad.quartz.model.AntadCredentials;
import com.addcel.antad.quartz.model.SftpVO;
import com.addcel.antad.quartz.model.TransaccionDetalleVO;
import com.addcel.antad.quartz.model.mapper.MCAntadBatchMapper;
import com.addcel.antad.quartz.utils.Log;
import com.addcel.antad.quartz.utils.MobilecardSFTPUtil;
import com.addcel.antad.quartz.utils.Utils;
import com.google.gson.Gson;

import crypto.Crypto;

@Service
public class MCAntadService {

	@Autowired
	private MCAntadBatchMapper mapper;
	
	@Autowired
	private MC11ClientImpl mc11Client;
	
	private Gson GSON = new Gson();

	public void iniciaSFTP(JobExecutionContext context) {
		String fileName = null;
		try {
			List<TransaccionDetalleVO> transacciones = mapper.getTransaccionesAntad();
			Log.info("[MC ANTAD BATCH] - TOTAL TRANSACCIONES EXITOSAS ENCONTRADAS: "+transacciones.size());
			List<TransaccionDetalleVO> transaccionesFallidas = mapper.getTransaccionesFallidas();
			Log.info("[MC ANTAD BATCH] - TOTAL TRANSACCIONES FALLIDAS ENCONTRADAS: "+transaccionesFallidas.size());
			List<TransaccionDetalleVO> transaccionesMC11 = getTransactionsMC11();
			Log.info("[MC ANTAD BATCH] - TOTAL TRANSACCIONESMC11  EXITOSAS ENCONTRADAS: "+transaccionesMC11.size());
			
			AntadCredentials credentials = mapper.getAntadCredentials();
			transacciones.addAll(transaccionesFallidas);
			transacciones.addAll(transaccionesMC11);
			Log.info("[MC ANTAD BATCH] - TOTAL TRANSACCIONES: "+transacciones.size());
			/*if(!transacciones.isEmpty()){*/
				SftpVO sftpVO = mapper.getSFTP("DATOS_FTP");
				FileManager file = new FileManager();
				fileName = file.makeFile(transacciones, credentials);
				MobilecardSFTPUtil sftpUtil = new MobilecardSFTPUtil();
				sftpVO.setFileName(fileName);
				sftpUtil.sendFileviaSFTP(sftpVO);
			/*} else {
				Log.info(this, "[MC ANTAD BATCH] - NO SE ENCONTRARON TRANSACCIONES");
			}*/
		} catch (Exception e) {
			e.printStackTrace();
			Log.error(this, "[MC ANTAD BATCH] - [ERROR] *** ERROR AL EJECUTAR EL JOB DE AFD ANTAD *** - CAUSA: "+ e.getCause());
		}
	}

	private List<TransaccionDetalleVO> getTransactionsMC11() {
		List<TransaccionDetalleVO> transMC11 = new ArrayList<TransaccionDetalleVO>();
		TransaccionDetalleVO detalle = null;
		Data data = null;
		AntadResponse antadData = null;
		DecimalFormat df = new DecimalFormat("0.00");
		try {
			
		    Date date = new Date(System.currentTimeMillis());			
			Calendar fechainicial = Calendar.getInstance();			
			Calendar fechafin = Calendar.getInstance();
			
			fechainicial.setTime(date);
			fechainicial.add(Calendar.DATE, -1);
			fechainicial.set(Calendar.HOUR_OF_DAY, 00);
			fechainicial.set(Calendar.MINUTE, 00);
			fechainicial.set(Calendar.SECOND, 00);
			fechainicial.set(Calendar.MILLISECOND, 00);
			
			
			fechafin.setTime(date);
			fechafin.add(Calendar.DATE, -1);			
			fechafin.set(Calendar.HOUR_OF_DAY, 23);
			fechafin.set(Calendar.MINUTE, 59);
			fechafin.set(Calendar.SECOND, 59);
			fechafin.set(Calendar.MILLISECOND, 59);
							
			Log.info("[Fecha inicio] "+ fechainicial.getTimeInMillis() +"[Fecha fin] "+fechafin.getTimeInMillis());
			data = mc11Client.getTransactions(fechainicial.getTimeInMillis()+"", fechafin.getTimeInMillis()+"");
			
			if(data.isSuccess()){
				TransactionMC11[] transactions = data.getData();
				for(TransactionMC11 transaction : transactions) {
					Log.info("MC 11 Transaction - {}", GSON.toJson(transaction));
					antadData = GSON.fromJson(transaction.getResponse(), AntadResponse.class);
					Log.info("MC 11 Transaction - Antad Response - {}", GSON.toJson(antadData));
					detalle = new TransaccionDetalleVO();
					detalle.setCajero("1");
					detalle.setCaja("1");
					detalle.setSucursal("1");
					detalle.setReferencia(transaction.getReference());
					detalle.setIdEmisor(transaction.getEmitter());
					detalle.setImporte(String.valueOf(df.format(transaction.getAmount())));
					detalle.setComision(Double.valueOf(antadData.getData().getComision())/100);
					detalle.setNumAutorizacion(antadData.getData().getNumAuth()+"");
					detalle.setFolioTransaccion(antadData.getData().getFolioTransaccion());
					detalle.setFolioComercio(antadData.getData().getFolioComercio());
					detalle.setCodigoRespuesta("00");
					detalle.setFechaTransaccion(Utils.getTransactionDate(transaction.getOnAsString()));
					detalle.setNumTarjeta(Crypto.aesEncrypt(Utils.parsePass("5525963513"), "000000000000"+transaction.getSourceCardSuffix()));
					transMC11.add(detalle);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return transMC11;
	}	

	public Date addDays(Date date, int days, int hours) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);
        cal.add(Calendar.HOUR, hours);
        return cal.getTime();
    }
	
	public Date addHours(Date date, int hours) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR, hours);
        return cal.getTime();
    }
	
}